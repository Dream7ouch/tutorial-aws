module.exports = {
  apps: [{
    name: 'tutorial-aws',
    script: './index.js'
  }],
  deploy: {
    production: {
      user: 'ubuntu',
      host: 'ec2-54-183-198-25.us-west-1.compute.amazonaws.com',
      key: '~/.ssh/AmazonTestEC2.pem',
      ref: 'origin/master',
      repo: 'git@gitlab.com:Dream7ouch/tutorial-aws.git',
      path: '/home/ubuntu/App/tutorial-aws',
      'post-deploy': 'npm install && pm2 startOrRestart ecosystem.config.js'
    }
  }
}
